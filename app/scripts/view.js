var $ = require('./domSelectors');

var View = {};

View.appendTextToEl = function(el, data) {
  return el.value = data;
};

View.changeValueOfEl = function(el, data) {
  return el.value = data;
};

View.randomName = function(player, randName) {
  if(player === 'player1') {
    View.changeValueOfEl($.randomNameOne(), randName);
  } else {
    View.changeValueOfEl($.randomNameTwo(), randName);
  }
};

View.removeWeapons = function(player) {
  $.byClass(player +'weapons')[0].style.display = 'none';
};

View.showWeapons = function(player) {
  $.byClass(player +'weapons')[0].style.display = '';
};

View.showLoser = function(player) {
  setTimeout(function() {
    if(player === 'player1') {
     $.addClass( $.byId('weaponA'), 'grayscale');
    } else {
      $.addClass( $.byId('weaponB'), 'grayscale');
    }
  }, 200);
};

View.updateScore = function(player1score, player2score) {
  $.byId('player1score').textContent = player1score;
  $.byId('player2score').textContent = player2score;
};

View.showPage = function(page) {
  var pageLength = $.page().length;
  for(var i=0;i<pageLength;i++) {
     $.removeClass($.page()[i], 'show');
  }
  $.addClass($.byClass(page)[0], 'show');
}

View.drawPage = function(player1, player2) {
     $.byId('player1namelabel').textContent = player1 || 'unknown';
     $.byId('player2namelabel').textContent = player2 || 'unknown';
};
 
module.exports = View;