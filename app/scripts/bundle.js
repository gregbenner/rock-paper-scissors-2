(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"./app/scripts/main.js":[function(require,module,exports){
var model = require('./model');
var view = require('./view');
var controller = require('./controller');


controller.init();
},{"./controller":"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/controller.js","./model":"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/model.js","./view":"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/view.js"}],"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/Model.js":[function(require,module,exports){
var Model = {};

Model.storageSet = function(key, value) {
  window.localStorage.setItem('rps-' + key, value);
}

Model.storageGet = function(key) {
  return window.localStorage.getItem('rps-' + key)
}

Model.player1Name = function() { return this.storageGet('player1Name')};
Model.player2Name =  function() { return this.storageGet('player2Name')};

module.exports = Model;
},{}],"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/View.js":[function(require,module,exports){
var $ = require('./domSelectors');

var View = {};

View.appendTextToEl = function(el, data) {
  return el.value = data;
};

View.changeValueOfEl = function(el, data) {
  return el.value = data;
};

View.randomName = function(player, randName) {
  if(player === 'player1') {
    View.changeValueOfEl($.randomNameOne(), randName);
  } else {
    View.changeValueOfEl($.randomNameTwo(), randName);
  }
};

View.removeWeapons = function(player) {
  $.byClass(player +'weapons')[0].style.display = 'none';
};

View.showWeapons = function(player) {
  $.byClass(player +'weapons')[0].style.display = '';
};

View.showLoser = function(player) {
  setTimeout(function() {
    if(player === 'player1') {
     $.addClass( $.byId('weaponA'), 'grayscale');
    } else {
      $.addClass( $.byId('weaponB'), 'grayscale');
    }
  }, 200);
};

View.updateScore = function(player1score, player2score) {
  $.byId('player1score').textContent = player1score;
  $.byId('player2score').textContent = player2score;
};

View.showPage = function(page) {
  var pageLength = $.page().length;
  for(var i=0;i<pageLength;i++) {
     $.removeClass($.page()[i], 'show');
  }
  $.addClass($.byClass(page)[0], 'show');
}

View.drawPage = function(player1, player2) {
     $.byId('player1namelabel').textContent = player1 || 'unknown';
     $.byId('player2namelabel').textContent = player2 || 'unknown';
};
 
module.exports = View;
},{"./domSelectors":"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/domSelectors.js"}],"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/controller.js":[function(require,module,exports){
var $ = require('./domSelectors');
var View = require('./View');
var Model = require('./Model');
var Controller = {};

Controller.init = function() {
  var _this = this;

  Model.storageSet('player1type', 'human');
  Model.storageSet('player2type', 'human');

  var randName =  function() {
    return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
  }
  $.randomButtonOne().addEventListener('click', function() {
    var randTemp = randName();
    View.randomName('player1', randTemp)
    Model.storageSet('player1Name', randTemp)
    }, false);
  $.randomButtonTwo().addEventListener('click', function() {
    var randTemp = randName();
    View.randomName('player2', randTemp)
    Model.storageSet('player2Name', randTemp)
    }, false);

  $.playGame().addEventListener('click', function() {
    Model.storageSet('currentPage', 'page2');
    View.showPage('page2');
    
    var player1human = function() {
      if(Model.storageGet('player1type') === 'human') {
        View.showWeapons('player1');
      }
      else {
        View.removeWeapons('player1');
      }
    }();

    var player2human = function() {
      if(Model.storageGet('player2type') === 'human') {
        View.showWeapons('player2');
        _this.computerRandomWeapon('player1');
      }
      else {
        View.removeWeapons('player2');
        _this.computerRandomWeapon('player2');
      }
    }();

    View.drawPage(Model.player1Name(), Model.player2Name());
  }, false);

  $.resetGame().addEventListener('click', function() {
    Model.storageSet('currentPage', 'page1');
    Model.storageSet('player1score', 0);
    Model.storageSet('player2score', 0);

    Model.storageSet('player1weapon', '');
    Model.storageSet('player2weapon', '');

    $.newClass($.byId('weaponA') ,'weapon')
    $.newClass($.byId('weaponB') ,'weapon')

    View.updateScore(0,0);

    View.showPage('page1');
  }, false);

  $.fightBtn().addEventListener('click', function() {
    _this.fightButton();
  }, false);

  var player1type = function() {
    var el = $.byClass('player1type');
    for (var i = 0, leng = el.length; i < leng; i++) {
      el[i].addEventListener('click', function() {
        Model.storageSet('player1type', this.value);
      }, false);
    };
  }();

  var player2type = function() {
    var el = $.byClass('player2type');
    for (var i = 0, leng = el.length; i < leng; i++) {
      el[i].addEventListener('click', function() {
        Model.storageSet('player2type', this.value);
      }, false);
    };
  }();

  var weaponSelection = function() {
    var playersWeapons = ['.player1weapons .weaponBtn', '.player2weapons .weaponBtn'];

      var weaponChoices = function(element) {
        var el = $.byAll(element);
        for (var i = 0, leng = el.length; i < leng; i++) {
          el[i].addEventListener('click', function() {
              Model.storageSet(this.dataset.player + 'weapon', this.dataset.weapon);
            }, false); 
        }
      };
      playersWeapons.forEach(weaponChoices);
  }();

  
}

Controller.fightButton = function() {
  var _this = this;
  var player1namelabel = $.byId('player1namelabel'),
      player2namelabel = $.byId('player2namelabel'),
      player1weapon = Model.storageGet('player1weapon'),
      player2weapon = Model.storageGet('player2weapon');
      $.removeClass(player1namelabel, 'error');
      $.removeClass(player2namelabel, 'error');

  if(Model.storageGet('player1type') === 'computer') {
    _this.computerRandomWeapon('player1');
    player1weapon = Model.storageGet('player1weapon')
  }
  if(Model.storageGet('player2type') === 'computer') {
    _this.computerRandomWeapon('player2');
    player2weapon = Model.storageGet('player2weapon')
  }

  (function() {
    if(player1weapon === '' || player1weapon == null) {
     $.addClass(player1namelabel, 'error');
    }
     if(player2weapon === '' || player2weapon == null) {
     $.addClass(player2namelabel, 'error');
    }
    return
  }());
  
  $.newClass($.byId('weaponA') ,'weapon');
  $.newClass($.byId('weaponB') ,'weapon');
  $.addClass($.byId('weaponA'), player1weapon);
  $.addClass($.byId('weaponB'), player2weapon);


  setTimeout(function() {
      _this.determineWinner(player1weapon, player2weapon);
      Model.storageSet('player1weapon', '');
      Model.storageSet('player2weapon', '');
  }, 200);

};

Controller.computerRandomWeapon = function(player) {
  var weapons = ['rock', 'paper', 'scissors'];
  var randWeapon = Math.floor(Math.random() * 2); 
  Model.storageSet(player + 'weapon', weapons[randWeapon]);
};

Controller.determineWinner = function(player1weapon, player2weapon) {
  if (player1weapon === player2weapon) {
    return;
  }


  var winner = function(player) {
    var loser;
    var currentScore = Model.storageGet(player + 'score');
    currentScore++;
    Model.storageSet(player + 'score', currentScore);
    player1score = Model.storageGet('player1score') || 0;
    player2score = Model.storageGet('player2score') || 0;
    View.updateScore(player1score, player2score);
    if(player === 'player1') {
      loser = 'player2'
    } else {
      loser = 'player1'
    }
    View.showLoser(loser);
  };

  if (player1weapon === 'rock') {
    if(player2weapon === 'paper') {
     winner('player2');
    } else {
      winner('player1');
    }
  }

  if (player1weapon === 'paper') {
    if(player2weapon === 'rock') {
       winner('player1');
    } else {
      winner('player2');
    }
  }

  if (player1weapon === 'scissors') {
    if(player2weapon === 'rock') {
       winner('player2');
    } else {
      winner('player1');
    }
  }
};

module.exports = Controller;
},{"./Model":"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/Model.js","./View":"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/View.js","./domSelectors":"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/domSelectors.js"}],"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/domSelectors.js":[function(require,module,exports){
DOM = {};

DOM.removeClass = function(el, className) {
  if (el.classList)
    el.classList.remove(className);
  else
    el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
   return this;
};

DOM.addClass = function(el, className) {
  if (el.classList)
  el.classList.add(className);
  else
    el.className += ' ' + className;
   return this;
};

DOM.newClass = function(el, className) {
  el.className = className;
  return this;
};

DOM.byAll = function(el) {
  return document.querySelectorAll(el);
}

DOM.byId = function(el) {
  return document.getElementById(el);
}

DOM.byClass = function(className) {
  return document.getElementsByClassName(className);
}

DOM.page = function() {
  return this.byClass('page')
};

DOM.randomNameOne = function() {
  return this.byId('player1name');
};
DOM.randomButtonOne =  function() {
  return this.byId('randomNameBtn1');
};
DOM.randomNameTwo = function() {
  return this.byId('player2name');
};
DOM.randomButtonTwo =  function() {
  return this.byId('randomNameBtn2');
};

DOM.fightBtn = function() {
  return this.byId('fightBtn');
};

DOM.playGame = function() {
  return this.byId('playGame');
};

DOM.resetGame = function() {
   return this.byId('resetGame');
 };

module.exports = DOM;
},{}],"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/model.js":[function(require,module,exports){
module.exports=require("/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/Model.js")
},{"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/Model.js":"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/Model.js"}],"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/view.js":[function(require,module,exports){
module.exports=require("/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/View.js")
},{"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/View.js":"/Users/greg/Sites3/rock-paper-scissors-2/app/scripts/View.js"}]},{},["./app/scripts/main.js"])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJhcHAvc2NyaXB0cy9tYWluLmpzIiwiYXBwL3NjcmlwdHMvTW9kZWwuanMiLCJhcHAvc2NyaXB0cy9WaWV3LmpzIiwiYXBwL3NjcmlwdHMvY29udHJvbGxlci5qcyIsImFwcC9zY3JpcHRzL2RvbVNlbGVjdG9ycy5qcyIsImFwcC9zY3JpcHRzL21vZGVsLmpzIiwiYXBwL3NjcmlwdHMvdmlldy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNMQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN4REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMxTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoRUE7O0FDQUEiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwidmFyIG1vZGVsID0gcmVxdWlyZSgnLi9tb2RlbCcpO1xudmFyIHZpZXcgPSByZXF1aXJlKCcuL3ZpZXcnKTtcbnZhciBjb250cm9sbGVyID0gcmVxdWlyZSgnLi9jb250cm9sbGVyJyk7XG5cblxuY29udHJvbGxlci5pbml0KCk7IiwidmFyIE1vZGVsID0ge307XG5cbk1vZGVsLnN0b3JhZ2VTZXQgPSBmdW5jdGlvbihrZXksIHZhbHVlKSB7XG4gIHdpbmRvdy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbSgncnBzLScgKyBrZXksIHZhbHVlKTtcbn1cblxuTW9kZWwuc3RvcmFnZUdldCA9IGZ1bmN0aW9uKGtleSkge1xuICByZXR1cm4gd2luZG93LmxvY2FsU3RvcmFnZS5nZXRJdGVtKCdycHMtJyArIGtleSlcbn1cblxuTW9kZWwucGxheWVyMU5hbWUgPSBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXMuc3RvcmFnZUdldCgncGxheWVyMU5hbWUnKX07XG5Nb2RlbC5wbGF5ZXIyTmFtZSA9ICBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXMuc3RvcmFnZUdldCgncGxheWVyMk5hbWUnKX07XG5cbm1vZHVsZS5leHBvcnRzID0gTW9kZWw7IiwidmFyICQgPSByZXF1aXJlKCcuL2RvbVNlbGVjdG9ycycpO1xuXG52YXIgVmlldyA9IHt9O1xuXG5WaWV3LmFwcGVuZFRleHRUb0VsID0gZnVuY3Rpb24oZWwsIGRhdGEpIHtcbiAgcmV0dXJuIGVsLnZhbHVlID0gZGF0YTtcbn07XG5cblZpZXcuY2hhbmdlVmFsdWVPZkVsID0gZnVuY3Rpb24oZWwsIGRhdGEpIHtcbiAgcmV0dXJuIGVsLnZhbHVlID0gZGF0YTtcbn07XG5cblZpZXcucmFuZG9tTmFtZSA9IGZ1bmN0aW9uKHBsYXllciwgcmFuZE5hbWUpIHtcbiAgaWYocGxheWVyID09PSAncGxheWVyMScpIHtcbiAgICBWaWV3LmNoYW5nZVZhbHVlT2ZFbCgkLnJhbmRvbU5hbWVPbmUoKSwgcmFuZE5hbWUpO1xuICB9IGVsc2Uge1xuICAgIFZpZXcuY2hhbmdlVmFsdWVPZkVsKCQucmFuZG9tTmFtZVR3bygpLCByYW5kTmFtZSk7XG4gIH1cbn07XG5cblZpZXcucmVtb3ZlV2VhcG9ucyA9IGZ1bmN0aW9uKHBsYXllcikge1xuICAkLmJ5Q2xhc3MocGxheWVyICsnd2VhcG9ucycpWzBdLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XG59O1xuXG5WaWV3LnNob3dXZWFwb25zID0gZnVuY3Rpb24ocGxheWVyKSB7XG4gICQuYnlDbGFzcyhwbGF5ZXIgKyd3ZWFwb25zJylbMF0uc3R5bGUuZGlzcGxheSA9ICcnO1xufTtcblxuVmlldy5zaG93TG9zZXIgPSBmdW5jdGlvbihwbGF5ZXIpIHtcbiAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICBpZihwbGF5ZXIgPT09ICdwbGF5ZXIxJykge1xuICAgICAkLmFkZENsYXNzKCAkLmJ5SWQoJ3dlYXBvbkEnKSwgJ2dyYXlzY2FsZScpO1xuICAgIH0gZWxzZSB7XG4gICAgICAkLmFkZENsYXNzKCAkLmJ5SWQoJ3dlYXBvbkInKSwgJ2dyYXlzY2FsZScpO1xuICAgIH1cbiAgfSwgMjAwKTtcbn07XG5cblZpZXcudXBkYXRlU2NvcmUgPSBmdW5jdGlvbihwbGF5ZXIxc2NvcmUsIHBsYXllcjJzY29yZSkge1xuICAkLmJ5SWQoJ3BsYXllcjFzY29yZScpLnRleHRDb250ZW50ID0gcGxheWVyMXNjb3JlO1xuICAkLmJ5SWQoJ3BsYXllcjJzY29yZScpLnRleHRDb250ZW50ID0gcGxheWVyMnNjb3JlO1xufTtcblxuVmlldy5zaG93UGFnZSA9IGZ1bmN0aW9uKHBhZ2UpIHtcbiAgdmFyIHBhZ2VMZW5ndGggPSAkLnBhZ2UoKS5sZW5ndGg7XG4gIGZvcih2YXIgaT0wO2k8cGFnZUxlbmd0aDtpKyspIHtcbiAgICAgJC5yZW1vdmVDbGFzcygkLnBhZ2UoKVtpXSwgJ3Nob3cnKTtcbiAgfVxuICAkLmFkZENsYXNzKCQuYnlDbGFzcyhwYWdlKVswXSwgJ3Nob3cnKTtcbn1cblxuVmlldy5kcmF3UGFnZSA9IGZ1bmN0aW9uKHBsYXllcjEsIHBsYXllcjIpIHtcbiAgICAgJC5ieUlkKCdwbGF5ZXIxbmFtZWxhYmVsJykudGV4dENvbnRlbnQgPSBwbGF5ZXIxIHx8ICd1bmtub3duJztcbiAgICAgJC5ieUlkKCdwbGF5ZXIybmFtZWxhYmVsJykudGV4dENvbnRlbnQgPSBwbGF5ZXIyIHx8ICd1bmtub3duJztcbn07XG4gXG5tb2R1bGUuZXhwb3J0cyA9IFZpZXc7IiwidmFyICQgPSByZXF1aXJlKCcuL2RvbVNlbGVjdG9ycycpO1xudmFyIFZpZXcgPSByZXF1aXJlKCcuL1ZpZXcnKTtcbnZhciBNb2RlbCA9IHJlcXVpcmUoJy4vTW9kZWwnKTtcbnZhciBDb250cm9sbGVyID0ge307XG5cbkNvbnRyb2xsZXIuaW5pdCA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3RoaXMgPSB0aGlzO1xuXG4gIE1vZGVsLnN0b3JhZ2VTZXQoJ3BsYXllcjF0eXBlJywgJ2h1bWFuJyk7XG4gIE1vZGVsLnN0b3JhZ2VTZXQoJ3BsYXllcjJ0eXBlJywgJ2h1bWFuJyk7XG5cbiAgdmFyIHJhbmROYW1lID0gIGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiBNYXRoLnJhbmRvbSgpLnRvU3RyaW5nKDM2KS5yZXBsYWNlKC9bXmEtel0rL2csICcnKS5zdWJzdHIoMCwgNSk7XG4gIH1cbiAgJC5yYW5kb21CdXR0b25PbmUoKS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAgIHZhciByYW5kVGVtcCA9IHJhbmROYW1lKCk7XG4gICAgVmlldy5yYW5kb21OYW1lKCdwbGF5ZXIxJywgcmFuZFRlbXApXG4gICAgTW9kZWwuc3RvcmFnZVNldCgncGxheWVyMU5hbWUnLCByYW5kVGVtcClcbiAgICB9LCBmYWxzZSk7XG4gICQucmFuZG9tQnV0dG9uVHdvKCkuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbigpIHtcbiAgICB2YXIgcmFuZFRlbXAgPSByYW5kTmFtZSgpO1xuICAgIFZpZXcucmFuZG9tTmFtZSgncGxheWVyMicsIHJhbmRUZW1wKVxuICAgIE1vZGVsLnN0b3JhZ2VTZXQoJ3BsYXllcjJOYW1lJywgcmFuZFRlbXApXG4gICAgfSwgZmFsc2UpO1xuXG4gICQucGxheUdhbWUoKS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAgIE1vZGVsLnN0b3JhZ2VTZXQoJ2N1cnJlbnRQYWdlJywgJ3BhZ2UyJyk7XG4gICAgVmlldy5zaG93UGFnZSgncGFnZTInKTtcbiAgICBcbiAgICB2YXIgcGxheWVyMWh1bWFuID0gZnVuY3Rpb24oKSB7XG4gICAgICBpZihNb2RlbC5zdG9yYWdlR2V0KCdwbGF5ZXIxdHlwZScpID09PSAnaHVtYW4nKSB7XG4gICAgICAgIFZpZXcuc2hvd1dlYXBvbnMoJ3BsYXllcjEnKTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBWaWV3LnJlbW92ZVdlYXBvbnMoJ3BsYXllcjEnKTtcbiAgICAgIH1cbiAgICB9KCk7XG5cbiAgICB2YXIgcGxheWVyMmh1bWFuID0gZnVuY3Rpb24oKSB7XG4gICAgICBpZihNb2RlbC5zdG9yYWdlR2V0KCdwbGF5ZXIydHlwZScpID09PSAnaHVtYW4nKSB7XG4gICAgICAgIFZpZXcuc2hvd1dlYXBvbnMoJ3BsYXllcjInKTtcbiAgICAgICAgX3RoaXMuY29tcHV0ZXJSYW5kb21XZWFwb24oJ3BsYXllcjEnKTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBWaWV3LnJlbW92ZVdlYXBvbnMoJ3BsYXllcjInKTtcbiAgICAgICAgX3RoaXMuY29tcHV0ZXJSYW5kb21XZWFwb24oJ3BsYXllcjInKTtcbiAgICAgIH1cbiAgICB9KCk7XG5cbiAgICBWaWV3LmRyYXdQYWdlKE1vZGVsLnBsYXllcjFOYW1lKCksIE1vZGVsLnBsYXllcjJOYW1lKCkpO1xuICB9LCBmYWxzZSk7XG5cbiAgJC5yZXNldEdhbWUoKS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAgIE1vZGVsLnN0b3JhZ2VTZXQoJ2N1cnJlbnRQYWdlJywgJ3BhZ2UxJyk7XG4gICAgTW9kZWwuc3RvcmFnZVNldCgncGxheWVyMXNjb3JlJywgMCk7XG4gICAgTW9kZWwuc3RvcmFnZVNldCgncGxheWVyMnNjb3JlJywgMCk7XG5cbiAgICBNb2RlbC5zdG9yYWdlU2V0KCdwbGF5ZXIxd2VhcG9uJywgJycpO1xuICAgIE1vZGVsLnN0b3JhZ2VTZXQoJ3BsYXllcjJ3ZWFwb24nLCAnJyk7XG5cbiAgICAkLm5ld0NsYXNzKCQuYnlJZCgnd2VhcG9uQScpICwnd2VhcG9uJylcbiAgICAkLm5ld0NsYXNzKCQuYnlJZCgnd2VhcG9uQicpICwnd2VhcG9uJylcblxuICAgIFZpZXcudXBkYXRlU2NvcmUoMCwwKTtcblxuICAgIFZpZXcuc2hvd1BhZ2UoJ3BhZ2UxJyk7XG4gIH0sIGZhbHNlKTtcblxuICAkLmZpZ2h0QnRuKCkuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbigpIHtcbiAgICBfdGhpcy5maWdodEJ1dHRvbigpO1xuICB9LCBmYWxzZSk7XG5cbiAgdmFyIHBsYXllcjF0eXBlID0gZnVuY3Rpb24oKSB7XG4gICAgdmFyIGVsID0gJC5ieUNsYXNzKCdwbGF5ZXIxdHlwZScpO1xuICAgIGZvciAodmFyIGkgPSAwLCBsZW5nID0gZWwubGVuZ3RoOyBpIDwgbGVuZzsgaSsrKSB7XG4gICAgICBlbFtpXS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAgICAgICBNb2RlbC5zdG9yYWdlU2V0KCdwbGF5ZXIxdHlwZScsIHRoaXMudmFsdWUpO1xuICAgICAgfSwgZmFsc2UpO1xuICAgIH07XG4gIH0oKTtcblxuICB2YXIgcGxheWVyMnR5cGUgPSBmdW5jdGlvbigpIHtcbiAgICB2YXIgZWwgPSAkLmJ5Q2xhc3MoJ3BsYXllcjJ0eXBlJyk7XG4gICAgZm9yICh2YXIgaSA9IDAsIGxlbmcgPSBlbC5sZW5ndGg7IGkgPCBsZW5nOyBpKyspIHtcbiAgICAgIGVsW2ldLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24oKSB7XG4gICAgICAgIE1vZGVsLnN0b3JhZ2VTZXQoJ3BsYXllcjJ0eXBlJywgdGhpcy52YWx1ZSk7XG4gICAgICB9LCBmYWxzZSk7XG4gICAgfTtcbiAgfSgpO1xuXG4gIHZhciB3ZWFwb25TZWxlY3Rpb24gPSBmdW5jdGlvbigpIHtcbiAgICB2YXIgcGxheWVyc1dlYXBvbnMgPSBbJy5wbGF5ZXIxd2VhcG9ucyAud2VhcG9uQnRuJywgJy5wbGF5ZXIyd2VhcG9ucyAud2VhcG9uQnRuJ107XG5cbiAgICAgIHZhciB3ZWFwb25DaG9pY2VzID0gZnVuY3Rpb24oZWxlbWVudCkge1xuICAgICAgICB2YXIgZWwgPSAkLmJ5QWxsKGVsZW1lbnQpO1xuICAgICAgICBmb3IgKHZhciBpID0gMCwgbGVuZyA9IGVsLmxlbmd0aDsgaSA8IGxlbmc7IGkrKykge1xuICAgICAgICAgIGVsW2ldLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgIE1vZGVsLnN0b3JhZ2VTZXQodGhpcy5kYXRhc2V0LnBsYXllciArICd3ZWFwb24nLCB0aGlzLmRhdGFzZXQud2VhcG9uKTtcbiAgICAgICAgICAgIH0sIGZhbHNlKTsgXG4gICAgICAgIH1cbiAgICAgIH07XG4gICAgICBwbGF5ZXJzV2VhcG9ucy5mb3JFYWNoKHdlYXBvbkNob2ljZXMpO1xuICB9KCk7XG5cbiAgXG59XG5cbkNvbnRyb2xsZXIuZmlnaHRCdXR0b24gPSBmdW5jdGlvbigpIHtcbiAgdmFyIF90aGlzID0gdGhpcztcbiAgdmFyIHBsYXllcjFuYW1lbGFiZWwgPSAkLmJ5SWQoJ3BsYXllcjFuYW1lbGFiZWwnKSxcbiAgICAgIHBsYXllcjJuYW1lbGFiZWwgPSAkLmJ5SWQoJ3BsYXllcjJuYW1lbGFiZWwnKSxcbiAgICAgIHBsYXllcjF3ZWFwb24gPSBNb2RlbC5zdG9yYWdlR2V0KCdwbGF5ZXIxd2VhcG9uJyksXG4gICAgICBwbGF5ZXIyd2VhcG9uID0gTW9kZWwuc3RvcmFnZUdldCgncGxheWVyMndlYXBvbicpO1xuICAgICAgJC5yZW1vdmVDbGFzcyhwbGF5ZXIxbmFtZWxhYmVsLCAnZXJyb3InKTtcbiAgICAgICQucmVtb3ZlQ2xhc3MocGxheWVyMm5hbWVsYWJlbCwgJ2Vycm9yJyk7XG5cbiAgaWYoTW9kZWwuc3RvcmFnZUdldCgncGxheWVyMXR5cGUnKSA9PT0gJ2NvbXB1dGVyJykge1xuICAgIF90aGlzLmNvbXB1dGVyUmFuZG9tV2VhcG9uKCdwbGF5ZXIxJyk7XG4gICAgcGxheWVyMXdlYXBvbiA9IE1vZGVsLnN0b3JhZ2VHZXQoJ3BsYXllcjF3ZWFwb24nKVxuICB9XG4gIGlmKE1vZGVsLnN0b3JhZ2VHZXQoJ3BsYXllcjJ0eXBlJykgPT09ICdjb21wdXRlcicpIHtcbiAgICBfdGhpcy5jb21wdXRlclJhbmRvbVdlYXBvbigncGxheWVyMicpO1xuICAgIHBsYXllcjJ3ZWFwb24gPSBNb2RlbC5zdG9yYWdlR2V0KCdwbGF5ZXIyd2VhcG9uJylcbiAgfVxuXG4gIChmdW5jdGlvbigpIHtcbiAgICBpZihwbGF5ZXIxd2VhcG9uID09PSAnJyB8fCBwbGF5ZXIxd2VhcG9uID09IG51bGwpIHtcbiAgICAgJC5hZGRDbGFzcyhwbGF5ZXIxbmFtZWxhYmVsLCAnZXJyb3InKTtcbiAgICB9XG4gICAgIGlmKHBsYXllcjJ3ZWFwb24gPT09ICcnIHx8IHBsYXllcjJ3ZWFwb24gPT0gbnVsbCkge1xuICAgICAkLmFkZENsYXNzKHBsYXllcjJuYW1lbGFiZWwsICdlcnJvcicpO1xuICAgIH1cbiAgICByZXR1cm5cbiAgfSgpKTtcbiAgXG4gICQubmV3Q2xhc3MoJC5ieUlkKCd3ZWFwb25BJykgLCd3ZWFwb24nKTtcbiAgJC5uZXdDbGFzcygkLmJ5SWQoJ3dlYXBvbkInKSAsJ3dlYXBvbicpO1xuICAkLmFkZENsYXNzKCQuYnlJZCgnd2VhcG9uQScpLCBwbGF5ZXIxd2VhcG9uKTtcbiAgJC5hZGRDbGFzcygkLmJ5SWQoJ3dlYXBvbkInKSwgcGxheWVyMndlYXBvbik7XG5cblxuICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgX3RoaXMuZGV0ZXJtaW5lV2lubmVyKHBsYXllcjF3ZWFwb24sIHBsYXllcjJ3ZWFwb24pO1xuICAgICAgTW9kZWwuc3RvcmFnZVNldCgncGxheWVyMXdlYXBvbicsICcnKTtcbiAgICAgIE1vZGVsLnN0b3JhZ2VTZXQoJ3BsYXllcjJ3ZWFwb24nLCAnJyk7XG4gIH0sIDIwMCk7XG5cbn07XG5cbkNvbnRyb2xsZXIuY29tcHV0ZXJSYW5kb21XZWFwb24gPSBmdW5jdGlvbihwbGF5ZXIpIHtcbiAgdmFyIHdlYXBvbnMgPSBbJ3JvY2snLCAncGFwZXInLCAnc2Npc3NvcnMnXTtcbiAgdmFyIHJhbmRXZWFwb24gPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAyKTsgXG4gIE1vZGVsLnN0b3JhZ2VTZXQocGxheWVyICsgJ3dlYXBvbicsIHdlYXBvbnNbcmFuZFdlYXBvbl0pO1xufTtcblxuQ29udHJvbGxlci5kZXRlcm1pbmVXaW5uZXIgPSBmdW5jdGlvbihwbGF5ZXIxd2VhcG9uLCBwbGF5ZXIyd2VhcG9uKSB7XG4gIGlmIChwbGF5ZXIxd2VhcG9uID09PSBwbGF5ZXIyd2VhcG9uKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cblxuICB2YXIgd2lubmVyID0gZnVuY3Rpb24ocGxheWVyKSB7XG4gICAgdmFyIGxvc2VyO1xuICAgIHZhciBjdXJyZW50U2NvcmUgPSBNb2RlbC5zdG9yYWdlR2V0KHBsYXllciArICdzY29yZScpO1xuICAgIGN1cnJlbnRTY29yZSsrO1xuICAgIE1vZGVsLnN0b3JhZ2VTZXQocGxheWVyICsgJ3Njb3JlJywgY3VycmVudFNjb3JlKTtcbiAgICBwbGF5ZXIxc2NvcmUgPSBNb2RlbC5zdG9yYWdlR2V0KCdwbGF5ZXIxc2NvcmUnKSB8fCAwO1xuICAgIHBsYXllcjJzY29yZSA9IE1vZGVsLnN0b3JhZ2VHZXQoJ3BsYXllcjJzY29yZScpIHx8IDA7XG4gICAgVmlldy51cGRhdGVTY29yZShwbGF5ZXIxc2NvcmUsIHBsYXllcjJzY29yZSk7XG4gICAgaWYocGxheWVyID09PSAncGxheWVyMScpIHtcbiAgICAgIGxvc2VyID0gJ3BsYXllcjInXG4gICAgfSBlbHNlIHtcbiAgICAgIGxvc2VyID0gJ3BsYXllcjEnXG4gICAgfVxuICAgIFZpZXcuc2hvd0xvc2VyKGxvc2VyKTtcbiAgfTtcblxuICBpZiAocGxheWVyMXdlYXBvbiA9PT0gJ3JvY2snKSB7XG4gICAgaWYocGxheWVyMndlYXBvbiA9PT0gJ3BhcGVyJykge1xuICAgICB3aW5uZXIoJ3BsYXllcjInKTtcbiAgICB9IGVsc2Uge1xuICAgICAgd2lubmVyKCdwbGF5ZXIxJyk7XG4gICAgfVxuICB9XG5cbiAgaWYgKHBsYXllcjF3ZWFwb24gPT09ICdwYXBlcicpIHtcbiAgICBpZihwbGF5ZXIyd2VhcG9uID09PSAncm9jaycpIHtcbiAgICAgICB3aW5uZXIoJ3BsYXllcjEnKTtcbiAgICB9IGVsc2Uge1xuICAgICAgd2lubmVyKCdwbGF5ZXIyJyk7XG4gICAgfVxuICB9XG5cbiAgaWYgKHBsYXllcjF3ZWFwb24gPT09ICdzY2lzc29ycycpIHtcbiAgICBpZihwbGF5ZXIyd2VhcG9uID09PSAncm9jaycpIHtcbiAgICAgICB3aW5uZXIoJ3BsYXllcjInKTtcbiAgICB9IGVsc2Uge1xuICAgICAgd2lubmVyKCdwbGF5ZXIxJyk7XG4gICAgfVxuICB9XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IENvbnRyb2xsZXI7IiwiRE9NID0ge307XG5cbkRPTS5yZW1vdmVDbGFzcyA9IGZ1bmN0aW9uKGVsLCBjbGFzc05hbWUpIHtcbiAgaWYgKGVsLmNsYXNzTGlzdClcbiAgICBlbC5jbGFzc0xpc3QucmVtb3ZlKGNsYXNzTmFtZSk7XG4gIGVsc2VcbiAgICBlbC5jbGFzc05hbWUgPSBlbC5jbGFzc05hbWUucmVwbGFjZShuZXcgUmVnRXhwKCcoXnxcXFxcYiknICsgY2xhc3NOYW1lLnNwbGl0KCcgJykuam9pbignfCcpICsgJyhcXFxcYnwkKScsICdnaScpLCAnICcpO1xuICAgcmV0dXJuIHRoaXM7XG59O1xuXG5ET00uYWRkQ2xhc3MgPSBmdW5jdGlvbihlbCwgY2xhc3NOYW1lKSB7XG4gIGlmIChlbC5jbGFzc0xpc3QpXG4gIGVsLmNsYXNzTGlzdC5hZGQoY2xhc3NOYW1lKTtcbiAgZWxzZVxuICAgIGVsLmNsYXNzTmFtZSArPSAnICcgKyBjbGFzc05hbWU7XG4gICByZXR1cm4gdGhpcztcbn07XG5cbkRPTS5uZXdDbGFzcyA9IGZ1bmN0aW9uKGVsLCBjbGFzc05hbWUpIHtcbiAgZWwuY2xhc3NOYW1lID0gY2xhc3NOYW1lO1xuICByZXR1cm4gdGhpcztcbn07XG5cbkRPTS5ieUFsbCA9IGZ1bmN0aW9uKGVsKSB7XG4gIHJldHVybiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGVsKTtcbn1cblxuRE9NLmJ5SWQgPSBmdW5jdGlvbihlbCkge1xuICByZXR1cm4gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoZWwpO1xufVxuXG5ET00uYnlDbGFzcyA9IGZ1bmN0aW9uKGNsYXNzTmFtZSkge1xuICByZXR1cm4gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShjbGFzc05hbWUpO1xufVxuXG5ET00ucGFnZSA9IGZ1bmN0aW9uKCkge1xuICByZXR1cm4gdGhpcy5ieUNsYXNzKCdwYWdlJylcbn07XG5cbkRPTS5yYW5kb21OYW1lT25lID0gZnVuY3Rpb24oKSB7XG4gIHJldHVybiB0aGlzLmJ5SWQoJ3BsYXllcjFuYW1lJyk7XG59O1xuRE9NLnJhbmRvbUJ1dHRvbk9uZSA9ICBmdW5jdGlvbigpIHtcbiAgcmV0dXJuIHRoaXMuYnlJZCgncmFuZG9tTmFtZUJ0bjEnKTtcbn07XG5ET00ucmFuZG9tTmFtZVR3byA9IGZ1bmN0aW9uKCkge1xuICByZXR1cm4gdGhpcy5ieUlkKCdwbGF5ZXIybmFtZScpO1xufTtcbkRPTS5yYW5kb21CdXR0b25Ud28gPSAgZnVuY3Rpb24oKSB7XG4gIHJldHVybiB0aGlzLmJ5SWQoJ3JhbmRvbU5hbWVCdG4yJyk7XG59O1xuXG5ET00uZmlnaHRCdG4gPSBmdW5jdGlvbigpIHtcbiAgcmV0dXJuIHRoaXMuYnlJZCgnZmlnaHRCdG4nKTtcbn07XG5cbkRPTS5wbGF5R2FtZSA9IGZ1bmN0aW9uKCkge1xuICByZXR1cm4gdGhpcy5ieUlkKCdwbGF5R2FtZScpO1xufTtcblxuRE9NLnJlc2V0R2FtZSA9IGZ1bmN0aW9uKCkge1xuICAgcmV0dXJuIHRoaXMuYnlJZCgncmVzZXRHYW1lJyk7XG4gfTtcblxubW9kdWxlLmV4cG9ydHMgPSBET007IiwibW9kdWxlLmV4cG9ydHM9cmVxdWlyZShcIi9Vc2Vycy9ncmVnL1NpdGVzMy9yb2NrLXBhcGVyLXNjaXNzb3JzLTIvYXBwL3NjcmlwdHMvTW9kZWwuanNcIikiLCJtb2R1bGUuZXhwb3J0cz1yZXF1aXJlKFwiL1VzZXJzL2dyZWcvU2l0ZXMzL3JvY2stcGFwZXItc2Npc3NvcnMtMi9hcHAvc2NyaXB0cy9WaWV3LmpzXCIpIl19
