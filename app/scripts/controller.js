var $ = require('./domSelectors');
var View = require('./View');
var Model = require('./Model');
var Controller = {};

Controller.init = function() {
  var _this = this;

  Model.storageSet('player1type', 'human');
  Model.storageSet('player2type', 'human');

  var randName =  function() {
    return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
  }
  $.randomButtonOne().addEventListener('click', function() {
    var randTemp = randName();
    View.randomName('player1', randTemp)
    Model.storageSet('player1Name', randTemp)
    }, false);
  $.randomButtonTwo().addEventListener('click', function() {
    var randTemp = randName();
    View.randomName('player2', randTemp)
    Model.storageSet('player2Name', randTemp)
    }, false);

  $.playGame().addEventListener('click', function() {
    Model.storageSet('currentPage', 'page2');
    View.showPage('page2');
    
    var player1human = function() {
      if(Model.storageGet('player1type') === 'human') {
        View.showWeapons('player1');
      }
      else {
        View.removeWeapons('player1');
      }
    }();

    var player2human = function() {
      if(Model.storageGet('player2type') === 'human') {
        View.showWeapons('player2');
        _this.computerRandomWeapon('player1');
      }
      else {
        View.removeWeapons('player2');
        _this.computerRandomWeapon('player2');
      }
    }();

    View.drawPage(Model.player1Name(), Model.player2Name());
  }, false);

  $.resetGame().addEventListener('click', function() {
    Model.storageSet('currentPage', 'page1');
    Model.storageSet('player1score', 0);
    Model.storageSet('player2score', 0);

    Model.storageSet('player1weapon', '');
    Model.storageSet('player2weapon', '');

    $.newClass($.byId('weaponA') ,'weapon')
    $.newClass($.byId('weaponB') ,'weapon')

    View.updateScore(0,0);

    View.showPage('page1');
  }, false);

  $.fightBtn().addEventListener('click', function() {
    _this.fightButton();
  }, false);

  var player1type = function() {
    var el = $.byClass('player1type');
    for (var i = 0, leng = el.length; i < leng; i++) {
      el[i].addEventListener('click', function() {
        Model.storageSet('player1type', this.value);
      }, false);
    };
  }();

  var player2type = function() {
    var el = $.byClass('player2type');
    for (var i = 0, leng = el.length; i < leng; i++) {
      el[i].addEventListener('click', function() {
        Model.storageSet('player2type', this.value);
      }, false);
    };
  }();

  var weaponSelection = function() {
    var playersWeapons = ['.player1weapons .weaponBtn', '.player2weapons .weaponBtn'];

      var weaponChoices = function(element) {
        var el = $.byAll(element);
        for (var i = 0, leng = el.length; i < leng; i++) {
          el[i].addEventListener('click', function() {
              Model.storageSet(this.dataset.player + 'weapon', this.dataset.weapon);
            }, false); 
        }
      };
      playersWeapons.forEach(weaponChoices);
  }();

  
}

Controller.fightButton = function() {
  var _this = this;
  var player1namelabel = $.byId('player1namelabel'),
      player2namelabel = $.byId('player2namelabel'),
      player1weapon = Model.storageGet('player1weapon'),
      player2weapon = Model.storageGet('player2weapon');
      $.removeClass(player1namelabel, 'error');
      $.removeClass(player2namelabel, 'error');

  if(Model.storageGet('player1type') === 'computer') {
    _this.computerRandomWeapon('player1');
    player1weapon = Model.storageGet('player1weapon')
  }
  if(Model.storageGet('player2type') === 'computer') {
    _this.computerRandomWeapon('player2');
    player2weapon = Model.storageGet('player2weapon')
  }

  (function() {
    if(player1weapon === '' || player1weapon == null) {
     $.addClass(player1namelabel, 'error');
    }
     if(player2weapon === '' || player2weapon == null) {
     $.addClass(player2namelabel, 'error');
    }
    return
  }());
  
  $.newClass($.byId('weaponA') ,'weapon');
  $.newClass($.byId('weaponB') ,'weapon');
  $.addClass($.byId('weaponA'), player1weapon);
  $.addClass($.byId('weaponB'), player2weapon);


  setTimeout(function() {
      _this.determineWinner(player1weapon, player2weapon);
      Model.storageSet('player1weapon', '');
      Model.storageSet('player2weapon', '');
  }, 200);

};

Controller.computerRandomWeapon = function(player) {
  var weapons = ['rock', 'paper', 'scissors'];
  var randWeapon = Math.floor(Math.random() * 2); 
  Model.storageSet(player + 'weapon', weapons[randWeapon]);
};

Controller.determineWinner = function(player1weapon, player2weapon) {
  if (player1weapon === player2weapon) {
    return;
  }


  var winner = function(player) {
    var loser;
    var currentScore = Model.storageGet(player + 'score');
    currentScore++;
    Model.storageSet(player + 'score', currentScore);
    player1score = Model.storageGet('player1score') || 0;
    player2score = Model.storageGet('player2score') || 0;
    View.updateScore(player1score, player2score);
    if(player === 'player1') {
      loser = 'player2'
    } else {
      loser = 'player1'
    }
    View.showLoser(loser);
  };

  if (player1weapon === 'rock') {
    if(player2weapon === 'paper') {
     winner('player2');
    } else {
      winner('player1');
    }
  }

  if (player1weapon === 'paper') {
    if(player2weapon === 'rock') {
       winner('player1');
    } else {
      winner('player2');
    }
  }

  if (player1weapon === 'scissors') {
    if(player2weapon === 'rock') {
       winner('player2');
    } else {
      winner('player1');
    }
  }
};

module.exports = Controller;