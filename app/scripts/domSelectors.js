DOM = {};

DOM.removeClass = function(el, className) {
  if (el.classList)
    el.classList.remove(className);
  else
    el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
   return this;
};

DOM.addClass = function(el, className) {
  if (el.classList)
  el.classList.add(className);
  else
    el.className += ' ' + className;
   return this;
};

DOM.newClass = function(el, className) {
  el.className = className;
  return this;
};

DOM.byAll = function(el) {
  return document.querySelectorAll(el);
}

DOM.byId = function(el) {
  return document.getElementById(el);
}

DOM.byClass = function(className) {
  return document.getElementsByClassName(className);
}

DOM.page = function() {
  return this.byClass('page')
};

DOM.randomNameOne = function() {
  return this.byId('player1name');
};
DOM.randomButtonOne =  function() {
  return this.byId('randomNameBtn1');
};
DOM.randomNameTwo = function() {
  return this.byId('player2name');
};
DOM.randomButtonTwo =  function() {
  return this.byId('randomNameBtn2');
};

DOM.fightBtn = function() {
  return this.byId('fightBtn');
};

DOM.playGame = function() {
  return this.byId('playGame');
};

DOM.resetGame = function() {
   return this.byId('resetGame');
 };

module.exports = DOM;