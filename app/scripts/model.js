var Model = {};

Model.storageSet = function(key, value) {
  window.localStorage.setItem('rps-' + key, value);
}

Model.storageGet = function(key) {
  return window.localStorage.getItem('rps-' + key)
}

Model.player1Name = function() { return this.storageGet('player1Name')};
Model.player2Name =  function() { return this.storageGet('player2Name')};

module.exports = Model;